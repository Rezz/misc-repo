import scrapy

from tutorial.items import UfsmItem

class UfsmSpider(scrapy.Spider):
    name = "ufsm"
    allowed_domains = ["http://sites.ufsm.br/"]
    start_urls = ["http://site.ufsm.br/"]

    def parse(self, response):
        for sel in response.xpath('//div[@class="slides"]/li'):
            #item = UfsmItem()
            link = sel.xpath('a/img/@src').extract()
            print link