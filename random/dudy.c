#include <stdio.h>

int main(void){

    float dias, i, pcdr;
    char tquarto[2], tlfn[2], tlvs[2], tipo[2];

    printf("Quantos dias voce ficou no hospital? ");
    scanf("%f", &dias);
    fflush(stdin); // limpa buffer de entrada para eliminar \n
    
    printf("Qual foi o tipo de quarto? P, S ou C?");
    scanf("%2[^\n]", tquarto); //vai ler no máximo dois caracteres para a variável até encontrar \n (enter)
    fflush(stdin);
    
    printf("Voce usou o telefone? ");
    gets(tlfn); // outro jeito de pegar string. Nao eh seguro, nao checa tamanho da variavel
    fflush(stdin);
    
    printf("E voce usou a televisao? ");
    fgets(tlvs, 20, stdin); // variavel em que armazena, tamanho da variavel, de onde pega (buffer de entrada)

    switch (tquarto){
        case 'p': case 'P':
            pcdr = dias*125;
            break;
        case 's': case 'S':
            pcdr = dias*95;
            break;
        case 'c': case 'C':
            pcdr = dias*75;
            break;
    }

    printf("HOSPITAL COMUNITARIO\n");
    printf("Numero de dias no hospital: %f", dias);
    printf("Tipo de quarto: %s", tipo);

    return 0;
}
