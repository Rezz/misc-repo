typedef struct lista Lista;

Lista* listaCriaEnc();
Lista* listaCriaCirc();
void listaInsere(Lista* p, int n);
void listaImprimeCirc(Lista* p);
void listaImprimeEnc(Lista* p);
int listaRemove(Lista* p);