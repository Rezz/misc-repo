#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "Lista.h"

int main(){
	Lista *LEnc, *LCirc;	// Declaração Lista Encadeada e Lista Circular
	int i, randomized;
	int tEnc, tCirc;		// Guarda o tamanho de cada lista

	srand(time(NULL));		// Seed do rand

	//Inicialização das listas
	LEnc = listaCriaEnc();		
	LCirc = listaCriaCirc();

	for(i = 0; i < 100; i++){
		randomized = rand() % 101;
		
		if(randomized % 2 == 0)
			if (randomized <= 50)
				listaInsere(LCirc, randomized);
			else
				listaRemove(LCirc);
		else
			if (randomized <= 50)
				listaInsere(LEnc, randomized);
			else
				listaRemove(LEnc);
	}
	
	printf("\n\n#### LISTA PAR / CIRCULAR");
	tCirc = listaCircChecaTamanho(LCirc);
	printf(" - TAMANHO: %d ####\n", tCirc);
	if (tCirc != 0)
		listaImprimeCirc(LCirc);
	else
		printf("\nLista vazia");
		
	printf("\n\n#### LISTA IMPAR / ENCADEADA");
	tEnc = listaEncChecaTamanho(LEnc);
	printf(" - TAMANHO: %d ####\n", tEnc);
	if (tEnc != 0)
		listaImprimeEnc(LEnc);
	else
		printf("\nLista vazia");
	
	
	printf("\n####Fim do Programa ####\n");
	return 0;
}
