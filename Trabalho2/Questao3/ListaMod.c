#include <stdio.h>
#include <stdlib.h>

struct lista{
	int info;
	struct lista* prox;
};

typedef struct lista Lista;

Lista* listaCriaEnc(){
	Lista* p = (Lista *) malloc(sizeof(Lista));

	p->prox = NULL;
	p->info = 0;

	return p;
}

Lista* listaCriaCirc(){
	Lista* novo = (Lista *) malloc(sizeof(Lista));
	
	novo->prox = novo;
	novo->info = 0;
	
	return novo;
}

void listaInsere(Lista* p, int n){
	Lista* novo = (Lista *) malloc(sizeof(Lista));
	
	novo->info = n;
	novo->prox = p->prox;
	p->prox = novo;
}

void listaImprimeCirc(Lista* p) {
	Lista *indice;
    
	for (indice = p->prox; indice != p; indice = indice->prox)
		printf ("%d\t", indice->info);
}

void listaImprimeEnc(Lista* p) {
	Lista *indice;
    
	for (indice = p->prox; indice != NULL; indice = indice->prox)
		printf ("%d\t", indice->info);
}

int listaEncChecaTamanho(Lista* p){
	Lista* indice;
	int cont = 0;
	
	for (indice = p->prox; indice != NULL; indice = indice->prox)
		cont++;
	
	return cont;
}

int listaCircChecaTamanho(Lista* p){
	Lista* indice;
	int cont = 0;
	
	for (indice = p->prox; indice != p; indice = indice->prox)
		cont++;
	
	return cont;
}

int listaRemove(Lista* p){
	Lista *del;
	int v;
	
	del = p->prox;
	if (del == NULL || del == p){
		printf("\nNao foi possivel remover da lista");
		return 0;
	}
	
	v = del->info;
	p->prox = del->prox;

	free(del);
	return v;
}
