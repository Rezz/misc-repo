typedef struct pilha Pilha;

Pilha* pilhaCria();
int pilhaCheca(Pilha* p);
int pilhaPush(Pilha* p, int n);
int pilhaPop(Pilha* p);
int pilhaTam(Pilha* p);