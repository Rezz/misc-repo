#include <stdio.h>

#include "Pilha.h"

int main(){
	Pilha  *pilhaInicial, *pilhaPar, *pilhaImpar;
	int i, ToPush, Popped, max;

	pilhaInicial = pilhaCria();
	pilhaPar = pilhaCria();
	pilhaImpar = pilhaCria();
	
	printf("\nInsira 10 valores para empilhar: ");
	for(i = 0; i < 10; i++){
		scanf("%d", &ToPush);
		pilhaPush(pilhaInicial, ToPush);
	}

	for(i = 0; i < 10; i++){
		Popped = pilhaPop(pilhaInicial);
		if (Popped % 2 == 0)
			pilhaPush(pilhaPar, Popped);
		else
			pilhaPush(pilhaImpar, Popped);
	}

	printf("\n\n#### Desempilhando pilha par ####\n");
	for(i = 0, max = pilhaTam(pilhaPar); i < max ; i++)
		printf("%d\n", pilhaPop(pilhaPar));


	printf("\n\n#### Desempilhando pilha impar ####\n");
	for(i = 0, max = pilhaTam(pilhaImpar); i < max ; i++)
		printf("%d\n", pilhaPop(pilhaImpar));

	return 0;
}
