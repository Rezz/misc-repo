#include <stdio.h>
#include <stdlib.h>

#include "Pilha.h"

#define TAM 10

struct pilha{
	int topo;
	int pos[TAM];
};

typedef struct pilha Pilha;

Pilha* pilhaCria(){
	Pilha* p = (Pilha *) malloc(sizeof(Pilha));
	p->topo = 0;

	return p;
}

int pilhaCheca(Pilha* p){
	if (p->topo == 0)
		return 1;
	else if (p->topo == TAM)
		return 2;
	else
		return 0;
}

int pilhaPush(Pilha* p, int n){
	if (pilhaCheca(p) == 2)
		return 0;
	p->pos[p->topo] = n;
	p->topo++;

	return 1;
}

int pilhaPop(Pilha* p){
	if(pilhaCheca(p) == 1)
		return -1;
	p->topo--;
	return p->pos[p->topo];
}

int pilhaTam(Pilha* p){
	return p->topo;
}
