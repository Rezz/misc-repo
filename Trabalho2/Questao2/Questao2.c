#include <stdio.h>
#include <math.h>

#include "Pilha.h"

int main(){
	Pilha* pA = pilhaCria();
	Pilha* pB = pilhaCria();
	int i, popped;

	for(i = 0; i < 10; i++)
		pilhaPush(pA, (rand() % 100) + 1);

	printf("#### PILHA A ####\n");
	for(i = 0; i < 10; i++){
		popped = pilhaPop(pA);
		printf("%d\n", popped);
		pilhaPush(pB, popped);
	}

	printf("\n#### PILHA B ####\n");
	for(i = 0; i < 10; i++)
		printf("%d\n", pilhaPop(pB));

	return 0;
}